const Graph = require("graph.js/dist/graph.full.js");

function uniq(a) {
  var seen = {};
  return a.filter(function(item) {
    return seen.hasOwnProperty(item) ? false : (seen[item] = true);
  });
}

const increasingPaths = function(inputGrid) {
  const columnCount =
    inputGrid.map(row => row.length).reduce((memo, el) => memo + el, 0) /
    inputGrid.length;

  if (columnCount !== inputGrid[0].length) {
    throw Error("Uneven grid - aborting");
  }

  const rowCount = inputGrid.length;

  // Assemble graph definition
  const [vertices, edges] = inputGrid.reduce(
    ([vertices, edges], rowGrid, rowIdx, grid) => {
      return rowGrid.reduce(
        (g, cell, colIdx) => {
          vertices.push([`${rowIdx},${colIdx}`, cell]);
          if (rowIdx - 1 >= 0 && cell < grid[rowIdx - 1][colIdx]) {
            edges.push([`${rowIdx},${colIdx}`, `${rowIdx - 1},${colIdx}`]);
          }
          if (rowIdx + 1 < rowCount && cell < grid[rowIdx + 1][colIdx]) {
            edges.push([`${rowIdx},${colIdx}`, `${rowIdx + 1},${colIdx}`]);
          }
          if (colIdx - 1 >= 0 && cell < grid[rowIdx][colIdx - 1]) {
            edges.push([`${rowIdx},${colIdx}`, `${rowIdx},${colIdx - 1}`]);
          }
          if (colIdx + 1 < columnCount && cell < grid[rowIdx][colIdx + 1]) {
            edges.push([`${rowIdx},${colIdx}`, `${rowIdx},${colIdx + 1}`]);
          }
          return [vertices, edges];
        },
        [vertices, edges]
      );
    },
    [[], []]
  );

  const g = new Graph();

  vertices.forEach(([index, cell]) => g.addNewVertex(index, cell));
  edges.forEach(([from, to]) => g.addNewEdge(from, to));

  const validPaths = [];

  // Generate paths
  for (let [sourceIndex] of g.sources()) {
    for (let [sinkIndex] of g.sinks()) {
      for (let path of g.paths(sourceIndex, sinkIndex)) {
        for (let size = 2; size <= path.length; size++) {
          for (let index = 0; index <= path.length; index++) {
            const s = path.slice(index, size);
            if (s.length > 1) {
              validPaths.push(s);
            }
          }
        }
      }
    }
  }

  return uniq(validPaths).map(row => row.map(key => g.vertexValue(key)));
};

const printPaths = paths =>
  paths
    .map(path => path.join(" → "))
    .sort()
    .join("\n");

module.exports = { increasingPaths, printPaths };
