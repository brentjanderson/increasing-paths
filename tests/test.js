const { increasingPaths, printPaths } = require("../src/index.js");
const assert = require("assert").strict;

const example1 = [[5, 1], [2, 7]];
const example2 = [[0, 4, 3], [5, 8, 9], [5, 9, 9]];
const example3 = [
  [5, 6, 10, 1, 5, 10],
  [54, 25, 29, 5, 4, 32],
  [23, 2, 1, 13, 26, 8],
  [10, 0, 2, 12, 14, 29]
];

const example4 = [
  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
  [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
  [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
  [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
  [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
  [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
  [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
  [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
  [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
  [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
  [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
  [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26],
  [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27],
  [14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]
];

const example1Expected = [[1, 5], [1, 7], [2, 5], [2, 7]];
const example2Expected = [
  [0, 4],
  [0, 4, 8],
  [0, 4, 8, 9],
  [0, 4, 8, 9],
  [0, 5],
  [0, 5, 8],
  [0, 5, 8, 9],
  [0, 5, 8, 9],
  [3, 4],
  [3, 4, 8],
  [3, 4, 8, 9],
  [3, 4, 8, 9],
  [3, 9],
  [4, 8],
  [4, 8, 9],
  [4, 8, 9],
  [5, 8],
  [5, 8, 9],
  [5, 8, 9],
  [5, 9],
  [8, 9],
  [8, 9]
];
const example3ExpectedLength = 96;
const example4ExpectedLength = 601079908;

const example1Result = increasingPaths(example1);
console.log(`Example 1 paths:
${printPaths(example1Result)}`);
assert.deepEqual(example1Result, example1Expected, "Example 1 doesn't match");

const example2Result = increasingPaths(example2);
console.log(`Example 2 paths:
${printPaths(example2Result)}`);
assert.deepEqual(
  example2Result.sort(),
  example2Expected.sort(),
  "Example 2 doesn't match"
);

const example3Result = increasingPaths(example3);
console.log(
  `Example 3 paths length: \n ${
    example3Result.length
  } (${example3ExpectedLength})`
);
assert.equal(
  example3Result.length,
  example3ExpectedLength,
  "Example 3 doesn't match"
);

// You may need to execute this test with --max_old_space_size=<a whole lot of megabytes>

// const example4Result = increasingPaths(example4);
// console.log(`Example 4 paths length: \n ${example4Result.length}`);
// assert.equal(
//   example4Result.length,
//   example4ExpectedLength,
//   "Example 4 doesn't match"
// );
