# Increasing paths

An increasing paths exercise. Given a grid of integers > 0, how many paths are there between cells going in increasing size?

## How to run

1. yarn
2. yarn test

## Methodology

The basic problem is essentially about constructing a directed graph that describes the ascending relationships between adjacent cells. My approach to this part of the problem is essentially two nested for loops. Even on the bonus example, this part runs super quick.

The slow part of my approach involves examining each "source" node (nodes with zero incoming connections") and each "sink" node, generating all paths between those nodes, expanding each path to each possible path with at least two nodes, and then deduplicating those node paths.

We finish by converting each vertex ID into the vertice's value for pretty printing.

Admittedly, I'm disappointed in the runtime performance on the larger sizes. I'm sure there's a data structure trick I'm blind to with this, although I'm confident that on a sufficiently equipped machine I could crunch through a graph of large enough size given enough time.

I constructed the code in a little over 1.5 hours using a TDD-based approach (basic test classes for the examples, `yarn test` and keep going until it works).

## Potential improvements

- Build chains of paths while constructing the graph so that each full path is fully constructed, then iterate over each path to produce the smaller segments of each path
- Building my own graph implementation using typed arrays for each path
  - This would help conserve memory and would be faster on analyzing subpaths, although more cumbersome to implement
- Eliminate need for the `length > 1` comparison in the nested for loop
- Assemble list of sources and sinks while assembling the graph parameters to skip introspecting the graph
- Re-write in a langauge meant for efficient processing (I'd reach for rust, although I'm a beginner)

## Alternative approaches

I considered using Elixir and, after constructing an immutable grid, spawning rows \* columns processes, each responsible for enumerating traversal paths. This would be inefficient, but would parallelize the work for computing each cell's neighbors and would make better use of a multi-core system for exploring each path.

I actually tried using Neo4j to do the heavy lifting of enumerating the relationships, however it ran out of memory on the bonus question. I was surprised by this result, given Neo4j is dedicated to graph processing.
